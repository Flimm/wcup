import requests
import re
import datetime


def get_matches(url: str, time: str | None = None, filter: str | None = None):
    request = requests.get(f"{url}/matches/{time or 'today'}{filter or ''}")
    if request.status_code != 200:
        yield (f"Error: {request.status_code}")
        return

    for i in request.json():
        # the fmt string
        home_team, away_team = i['home_team'], i['away_team']
        fmt_lhs = f"{home_team['name']} {str(home_team['goals'] or 0)}"
        fmt_rhs = f"{str(away_team['goals'] or 0)} {away_team['name']}"
        date = None
        if i["status"] == "future_scheduled":
            date = datetime.datetime.strptime(i["datetime"],
                                              "%Y-%m-%dT%H:%M:%SZ")

        if home_team["country"] == i["winner_code"]:
            fmt_lhs = "".join(("\x1b[32m", fmt_lhs))
        elif away_team["country"] == i["winner_code"]:
            fmt_rhs = "".join(("\x1b[32m", fmt_rhs))

        fmt_str = f"\x1b[4m{fmt_lhs}\x1b[0m VS \x1b[4m{fmt_rhs}\x1b[0m"

    # I would like to dearly apologize for this absolute monstrosity
    # It should be thrown into the next lava pool i know.
    # Ask the backyardscientist for availability!
    # TODO: Make this better, it needs it. It needs it very much.
        yield fmt_str + \
            "\n\x1b[2;m{:^{length}}\x1b[22;m\n".format(date.strftime("%H:%M") if date else None or " ".join(map(str.capitalize,
                                                                                                                i["status"]
                                                                                                                .replace("_", " ")
                                                                                                                .split(" "))),
                                                       length=len(re.sub(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])', '', fmt_str)))


if __name__ == "__main__":
    from .constants import URL
    for i in get_matches(URL):
        print(i)
