from . import get
from .constants import URL


def main():
    matches = get.get_matches(URL)
    if not any(matches):
        print("\x1b[2mNothing here today\x1b[0m")
        return
              
    for i in matches:
        print(i)


if __name__ == "__main__":
    main()
